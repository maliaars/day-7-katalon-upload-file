<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Internal Server Error</name>
   <tag></tag>
   <elementGuidId>3ea2a86f-ed2b-4052-bb12-2d9df2c9750e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='/html[1]/body[1]/h1[1]'])[1]/preceding::h1[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>c6a69fa9-2613-4245-b501-833bcd12c8ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Internal Server Error</value>
      <webElementGuid>071b3336-9062-4570-8531-5073121cde6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/h1[1]</value>
      <webElementGuid>5418f7ab-4045-49d7-85b3-54207af9b691</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='/html[1]/body[1]/h1[1]'])[1]/preceding::h1[1]</value>
      <webElementGuid>50df3b35-09f8-4539-af10-1467eed1ec4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Internal Server Error']/parent::*</value>
      <webElementGuid>4f9dba05-c199-4b95-83e3-2ecc7a223837</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>8adab8a2-6bc8-417e-b09b-b88584ce4ec1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Internal Server Error' or . = 'Internal Server Error')]</value>
      <webElementGuid>24b78f13-aad0-439f-817e-1932d71496be</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
