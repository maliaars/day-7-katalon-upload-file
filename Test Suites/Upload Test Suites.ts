<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Upload Test Suites</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>53b00f39-a52b-4de0-acec-4ca19c8dc0a3</testSuiteGuid>
   <testCaseLink>
      <guid>82c99616-7832-4af8-9116-354081f7cf1a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC002 - Upload File with Data Binding</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>a7ae96f3-455d-47d5-8376-3d1ee6147dfb</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/datafiles</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>a7ae96f3-455d-47d5-8376-3d1ee6147dfb</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>path</value>
         <variableId>b5c580bf-3c98-4f56-9bd4-4293147a0fa8</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
